<?php



//add_action( 'plugins_loaded', 'copy_publish_date_into_rel_date' );

function copy_publish_date_into_rel_date() {
	
	$args = array( 'post_type' => 'eme',
					'posts_per_page' => -1
					);
	
	$eme_posts = get_posts($args);
	
	foreach($eme_posts as $eme_post) {
		
		
		$post_date_timecode = strtotime($eme_post->post_date);
		$post_date = date( 'Ymd', $post_date_timecode );
		
		
		update_post_meta( $eme_post->ID, 'relevant_date', $post_date );
			
	}
	
}



?>