<?php


function eme_css_register_submenu_page() {
	add_theme_page( 'EduDMS EME Appearance', 'EME Appearance', 'edit_theme_options', 'eme_css_menu', 'eme_css_render_submenu_page' );
}
add_action( 'admin_menu', 'eme_css_register_submenu_page' );




function eme_css_register_settings() {
	register_setting( 'eme_css_settings_group', 'eme_css_settings' );
}
add_action( 'admin_init', 'eme_css_register_settings' );


function eme_css_render_submenu_page() {

	$options = get_option( 'eme_css_settings' );
	$content = isset( $options['eme_css-content'] ) && ! empty( $options['eme_css-content'] ) ? $options['eme_css-content'] : __( '/* Enter Your Custom CSS Here */', 'eme-css' );

	if ( isset( $_GET['settings-updated'] ) ) : ?>
		<div id="message" class="updated"><p>Custom CSS updated successfully.</p></div>
	<?php endif; ?>
	<div class="wrap">
		<h2 style="margin-bottom: 1em;">CSS Settings</h2>
		<form name="eme_css-form" action="options.php" method="post" enctype="multipart/form-data">
			<?php settings_fields( 'eme_css_settings_group' ); ?>
			<div id="templateside">
				<?php do_action( 'eme_css-sidebar-top' ); ?>
				<p style="margin-top: 0">Adjust the css of the eme addon</p>
				<p>To use, read comments carefully, alter hex values (#???), then click "Update EME CSS".</p>
				<?php submit_button( 'Update EME CSS', 'primary', 'submit', true ); ?>
				<?php do_action( 'eme_css-sidebar-bottom' ); ?>
			</div>
			<div id="template">
				<?php do_action( 'eme_css-form-top' ); ?>
				<div>
					<textarea cols="70" rows="30" name="eme_css_settings[eme_css-content]" id="eme_css_settings[eme_css-content]" ><?php echo esc_html( $content ); ?></textarea>
				</div>
				<?php do_action( 'eme_css-textarea-bottom' ); ?>
				<div>
					<?php submit_button( 'Update EME CSS', 'primary', 'submit', true ); ?>
				</div>
				<?php do_action( 'eme_css-form-bottom' ); ?>
			</div>
		</form>
		
	</div>

<?php
}



function eme_css_register_style() {
	$url = home_url();

	if ( is_ssl() ) {
		$url = home_url( '/', 'https' );
	}

	wp_register_style( 'eme_css_style', add_query_arg( array( 'eme_css' => 1 ), $url ) );

	wp_enqueue_style( 'eme_css_style' );
}
add_action( 'wp_enqueue_scripts', 'eme_css_register_style', 99 );

/**
 * If the query var is set, print the Simple Custom CSS rules.
 */
function eme_css_maybe_print_css() {

	// Only print CSS if this is a stylesheet request
	if( ! isset( $_GET['eme_css'] ) || intval( $_GET['eme_css'] ) !== 1 ) {
		return;
	}

	ob_start();
	header( 'Content-type: text/css' );
	$options     = get_option( 'eme_css_settings' );
	$raw_cont = isset( $options['eme_css-content'] ) ? $options['eme_css-content'] : '';
	$content     = wp_kses( $raw_cont, array( '\'', '\"' ) );
	$content     = str_replace( '&gt;', '>', $content );
	echo $content;
	die();
}

add_action( 'plugins_loaded', 'eme_css_maybe_print_css' );



?>