<?php



add_shortcode ('events', 'edudms_eme_shortcode');
add_shortcode ('eme_widget', 'edudms_eme_show');


function edudms_eme_show($atts) {
	
	$shortcode_atts = shortcode_atts( array(
        
    ), $atts );


	
	$events = edudms_eme_get();
	
//var_dump($events);
foreach ( $events as $event ) {
			$location =  EM_Locations::get( array( 'event' => $event->event_id ) );
			//print_r($location);
			 ?>
			<div class="events event-block">
				<div class="image">
					<a href="<?php echo get_post_permalink($event->post_id); ?>">
						<?php echo get_the_post_thumbnail($event->post_id, 'thumbnail'); ?>
					</a>
				</div>
				<div class="title">
						<a href="<?php echo get_post_permalink($event->post_id); ?>">
							<?php echo $event->event_name; ?>
						</a>
				</div>
				<?php echo date('l, F j, Y', $event->start); ?> @ 
				<?php foreach($location as $location1) { echo $location1->location_name; } ?>
			</div>
		 
		 
		 
		 
		 <?php
		}

	
	
}




function edudms_eme_shortcode($atts) {
ob_start();
edudms_eme_show($atts);
$eme_buffer = ob_get_clean();
	return $eme_buffer;
}



?>