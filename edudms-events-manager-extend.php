<?php
/*
Plugin Name: Edudemia EME Addon
Plugin URI: 
Description: Extend event management in your DMS
Version: 1.0.0
Author: Tyler Pruitt
Author URI:
Bitbucket Plugin URI: greatyler/edudemia-eme
Bitbucket Branch: realmaster

Credits:
This plugin's director of development and primary developer is Tyler Pruitt.

The following plugins were sourced or referrenced in this project:
Code from:



Dependencies or Referrences:





License

Edudemia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Edudemia software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
See http://www.gnu.org/licenses/old_licenses/gpl_2.0.en.html.
*/

include(dirname( __FILE__ ) . '/core.php');
include(dirname( __FILE__ ) . '/plugins.php');
include(dirname( __FILE__ ) . '/widget.php');
include(dirname( __FILE__ ) . '/shortcode.php');




//Activation hooks
register_activation_hook( __FILE__, 'edudms_eme_activate' );

function edudms_eme_activate() {
	
}


function edudms_eme_css_registration() {
wp_register_style('edudms_eme', plugins_url('css/eme.css',__FILE__ ));
wp_enqueue_style('edudms_eme');

}
add_action( 'wp_enqueue_scripts','edudms_eme_css_registration');




//Add EME Templates








	




//add eme template

function add_eme_template( $single_template )
{
	$object = get_queried_object();
	$single_postType_postName_template = locate_template(dirname(__FILE__) . "/single-event.php");
	if( file_exists( $single_postType_postName_template ) )
	{
		return $single_postType_postName_template;
	} else {
		return $single_template;
	}
}
add_filter( 'single_eme_template', 'add_eme_template', 10, 1 );















?>